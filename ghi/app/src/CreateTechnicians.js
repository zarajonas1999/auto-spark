import React, {useEffect, useState} from 'react';


function CreateTechnicians () {

    const [firstName,setFirstName] = useState('');
    const handleFirstNameChange = (event) => {
        const value = event.target.value;
        setFirstName(value);
    };

    const[lastName,setLastName] = useState('');
    const handleLastNameChange = (event) => {
        const value = event.target.value;
        setLastName(value);
    };

    const[employeeID,setEmployeeID] = useState('');
    const handleEmployeeIDChange = (event) => {
        const value = event.target.value;
        setEmployeeID(value);
    };

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.first_name = firstName;
        data.last_name = lastName;
        data.employee_id = employeeID;

        const techniciansURL = 'http://localhost:8080/api/technicians/';
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {'Content-Type': 'application/json',},
        };
        const response = await fetch(techniciansURL, fetchConfig);
        if (response.ok){
            setFirstName('');
            setLastName('');
            setEmployeeID('');
            event.target.reset();
        };
    };

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new technician</h1>
            <form
            id="create-technician-form"
            onSubmit = {handleSubmit}
            >
              <div className="form-floating mb-3">
                <input
                onChange={handleFirstNameChange}
                value={firstName}
                placeholder="Name"
                required type="text"
                name="first_name"
                id="first_name"
                className="form-control"
                />
                <label htmlFor="first_name">First name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                onChange={handleLastNameChange}
                value={lastName}
                placeholder="Last name"
                required type="text"
                name="last_name"
                id="last_name"
                className="form-control"
                />
                <label htmlFor="last_name">Last name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                onChange={handleEmployeeIDChange}
                value={employeeID}
                placeholder="Employee ID"
                required type="text"
                name="employee_id"
                id="employee_id"
                className="form-control"
                />
                <label htmlFor="employee_id">Employee ID</label>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
}






export default CreateTechnicians;

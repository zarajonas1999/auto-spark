import React, { useState, useEffect } from 'react';

function ListAutomobiles() {

    const [technicians, setTechnicians] = useState([]);
    const fetchData = async () => {
        const getURL = 'http://localhost:8100/api/automobiles/';
        const response = await fetch(getURL);
        if (response.ok) {
          const data = await response.json();
          setTechnicians(data.autos);
        };
      };

    useEffect(() => {
        fetchData();
    }, []);


    const isSold = (x) => {
        return x.sold ? 'yes' : 'no';
    }


    return (
        <table className='table'>
            <thead>
                <tr>
                    <th>VIN</th>
                    <th>Color</th>
                    <th>Year</th>
                    <th>Model</th>
                    <th>Manufacturer</th>
                    <th>Sold</th>
                </tr>
            </thead>
            <tbody>
                {technicians.map(technician => (
                    <tr key={technician.id}>
                        <td>{technician.vin}</td>
                        <td>{technician.color}</td>
                        <td>{technician.year}</td>
                        <td>{technician.model.name}</td>
                        <td>{technician.model.manufacturer.name}</td>
                        <td>{isSold(technician.sold)}</td>
                    </tr>
                ))}
            </tbody>
        </table>
    );
}

export default ListAutomobiles;

import React, {useEffect, useState} from 'react';


function CreateManufacturers () {

    const [manufacturerName,setManufacturerName] = useState('');
    const handleManufacturerNameChange = (event) => {
        const value = event.target.value;
        setManufacturerName(value);
    };

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.name = manufacturerName;

        const manufacturersURL = 'http://localhost:8100/api/manufacturers/';
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {'Content-Type': 'application/json',},
        };
        const response = await fetch(manufacturersURL, fetchConfig);
        if (response.ok){
            setManufacturerName('');
            event.target.reset();
        };
    };

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new manufacturer</h1>
            <form
            id="create-technician-form"
            onSubmit = {handleSubmit}
            >
              <div className="form-floating mb-3">
                <input
                onChange={handleManufacturerNameChange}
                value={manufacturerName}
                placeholder="Name"
                required type="text"
                name="name"
                id="name"
                className="form-control"
                />
                <label htmlFor="name">Manufacturer name</label>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
}






export default CreateManufacturers;
